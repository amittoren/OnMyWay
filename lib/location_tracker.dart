import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:on_my_way/globals.dart' as globals;

class LocationTracker extends StatefulWidget {


  LocationTrackerState createState() => LocationTrackerState();
}

class LocationTrackerState extends State<LocationTracker> {
  static const platform = const MethodChannel('location.tracker.service');
  List<globals.SavedContact> _contactQueue;

  @override
  initState() {
    super.initState();
    _contactQueue = new List();
  }

  Future<void> _startTracking() async {
    try {
      await platform.invokeMethod("startTracking");
    } on PlatformException catch (e) {

    }
  }

  addContactToQueue(globals.SavedContact contact) {
    _contactQueue.add(contact);
  }

  removeContactToQueue(globals.SavedContact contact) {
    _contactQueue.remove(contact);
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }
}